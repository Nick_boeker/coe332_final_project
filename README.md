# README #

The project is an http interface to a dataset containing the number of sunspots observed per year from 1770 to 1869.  Using Flask and JSON, the interface has a variety of endpoints that are accessible to the user and use standardized JSON for responses to improve communication between the interface and the user.

### Participants ###

Nicholas Boeker  
A. Lucero Herrera  

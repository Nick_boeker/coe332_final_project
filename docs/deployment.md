### Deployment Documentation ###

One Virtual Machine 
------

Simply docker-compose up on the machine you wish to run the system on.  The docker image will automatically pull from dockerhub and everything required will run automatically.

Two (or more) Virtual Machines 
------

On the main host machine run the basic docker-compose up to start the system.  To deploy to other machines while still accessing the main system, edit the docker-compose-child.yml file and alter the REDIS_IP and REDIS_PORT to match what you used on the main system, then run  
`
docker-compose -f docker-compose-child.yml up  
`    
to set up an api with access to the main system's database.

## Implemented Endpoints ##

### GET /sunspots ###
Return the entire dataset as a JSON list with each item as a dictionary.
```
GET /sunspots
```
```json
[  
  {  
    "id": 0,  
    "spots": 101,
    "year": 1770
  },  
  {  
    "id": 1,
    "spots": 82,  
    "year": 1771  
  },
  ...
  ...
  ...
  ...
]  
```

### GET /sunspots?start=<startyear>&end=<endyear> ###
Return the range of years specified by the arguments which set the *start* and *end* of the return as a JSON list. Either argument can be used independently but neither can be used in conjunction with *limit* or *offset*.  
```
GET /sunspots?start=1851&end=1854
```
```json
[
  {
    "id": 81,
    "spots": 65,
    "year": 1851
  },
  {
    "id": 82,
    "spots": 54,
    "year": 1852
  },
  {
    "id": 83,
    "spots": 39,
    "year": 1853
  },
  {
    "id": 84,
    "spots": 21,
    "year": 1854
  }
]
```

### GET /sunspots?limit=<limit>&offset=<offset> ###
Return the range of years bounded by the arguments which specify how many years to *offset* from the beginning of the dataset and how many entries (*limit*) to return as a JSON list. Either argument can be used independently but neither can be used in conjunction with *start* or *end*.  
```
GET /sunspots?limit=3&offset=5
```
```json
[
  {
    "id": 5,
    "spots": 7,
    "year": 1775
  },
  {
    "id": 6,
    "spots": 20,
    "year": 1776
  },
  {
    "id": 7,
    "spots": 93,
    "year": 1777
  }
]
```

### GET /sunspots/<id> ###
Return the dataset entry from the requested ID as a 1 item JSON list.
```
GET /sunspots/4
```
```json
[
  {
    "id": 4,
    "spots": 31,
    "year": 1774
  }
]
```

### GET /sunspots/year/<year> ###
Return the dataset entry from the requested year as a 1 item JSON list.
```
GET /sunspots/year/1849
```
```json
[
  {
    "id": 79,
    "spots": 96,
    "year": 1849
  }
]
```

### POST /jobs ### 
Post a job to return the selected year ranges. Also works with limit/offset, as in GET requests above.

```
POST {"start": 1770, “end”: 1839} /jobs
```
```json
{
  "create_time": "2018-12-14 05:33:38.974311",
  "end": 1839,
  "id": "6dfc4d87-9904-426b-8c9a-6a612e44a31a",
  "last_update_time": "2018-12-14 05:33:38.974311",
  "start": 1770,
  "status": "submitted"
}
```

### GET /jobs ###
Return the list of all present jobs. 
```
GET /jobs
```
```json
[
  {
    "create_time": "2018-12-14 05:33:38.974311",
    "end": "1839",
    "id": "6dfc4d87-9904-426b-8c9a-6a612e44a31a",
    "last_update_time": "2018-12-14 05:33:38.974311",
    "start": "1770",
    "status": "complete"
  },
  {
    "create_time": "2018-12-14 05:34:28.463045",
    "end": "1839",
    "id": "79e99cb7-e6d3-491c-afde-f4c529e063c1",
    "last_update_time": "2018-12-14 05:34:28.463045",
    "start": "1770",
    "status": "complete"
  }
]
```

### GET /jobs/<job_id> ###
Return the information for one job, given it's job id.  
```
GET /jobs/79e99cb7-e6d3-491c-afde-f4c529e063c1
```
```json
{
  "create_time": "2018-12-14 05:34:28.463045",
  "end": "1839",
  "id": "79e99cb7-e6d3-491c-afde-f4c529e063c1",
  "last_update_time": "2018-12-14 05:34:28.463045",
  "start": "1770",
  "status": "complete"
}
```

### GET /jobs/<jobs_id>/stats ###
Return the min, max, mean, and average sunspot occurances corresponding to a given job. 

```
GET /jobs/79e99cb7-e6d3-491c-afde-f4c529e063c1/stats
```
```json
{
  "maximum": 154,
  "mean": 45.98571428571429,
  "median": 35.5,
  "minimum": 0
}
```

### GET /jobs/<jobs_id>/plot###
Return the plot associated with job with jid == <jobs_id>.
```
GET /jobs/79e99cb7-e6d3-491c-afde-f4c529e063c1/plot >> output.file
```
The plot is returned in bytes as the output.


### POST /sunspots/year/newdata ### 
```
POST {"spots": 90, “year”: 2001} /sunspots/newyear
```
```
"Successful append"
```

## Examples ##
The following examples show how to access the interface using Curl or Python and show the expected output that goes with it.

### Curl ###
```bash
curl "localhost:5000/sunspots?limit=2&offset=38"
```
NOTE: If you're using either set of 2 arguments, make sure you enclose the address in quotations to avoid the *&* causing the bash terminal to hang.
```json
[
  {
    "id": 38,
    "spots": 8,
    "year": 1808
  },
  {
    "id": 39,
    "spots": 3,
    "year": 1809
  }
]
```

### Python ###
```python
import requests
r = requests.get('http://localhost:5000/sunspots/year/1867')
r.json()
```
The last line of the code above translates the json recieved (if valid) into python variables, in this case a list with dictionaries as items.
```json
[
  {
    "id": 97,
    "spots": 7,
    "year": 1867
  }
]
```

### Charges ###
Each customer will be charged a 5USD flat rate per month for use of the services detailed above. This covers costs to run the server, as well as pay for the time the server is occupied fulfilling a request.
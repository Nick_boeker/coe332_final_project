FROM python:3.6

ADD source/hw1.py /
ADD source/hw3.py /
ADD source/hw8.py /
ADD source/worker.py /
ADD source/sunspots.csv /
ADD requirements.txt /

RUN pip install -r requirements.txt

ENTRYPOINT ["python3"]
CMD ["hw3.py"]

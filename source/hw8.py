import uuid
from datetime import datetime 
import redis 
from hotqueue import HotQueue
import matplotlib.pyplot as plt
from hw1 import get_range_years,data_start,data_end
import os

DATA_DB = 0
QUEUE_DB = 1
SUBMITTED_STATUS = 'submitted'
IN_PROGRESS = 'in_progress'
COMPLETE_STATUS = 'complete'

REDIS_IP=os.environ.get('REDIS_IP')
REDIS_PORT=os.environ.get('REDIS_PORT')

#problem 1: generating a job object 
#helper functions:
def generate_jid():
    return str(uuid.uuid4())

def generate_job_key(jid):
    return 'job.{}'.format(jid)

def instantiate_job(jid,status = "submitted",start = 0,end = 0):
    create_time = str(datetime.now())
    last_update = create_time
    return {"id" : jid,
            "status": status,
            "start" : start,
            "end" : end,
            "create_time" : create_time,
            "last_update_time" : last_update}

#problem 2: adding job to redis dataset
rd = redis.StrictRedis(host=REDIS_IP, port=REDIS_PORT, db=DATA_DB)
q = HotQueue("queue",host=REDIS_IP, port=REDIS_PORT, db=QUEUE_DB)
rd.flushdb()
q.clear()
def add_job(jid,status = "submitted",start = data_start,end = data_end):
    #rd = redis.StrictRedis(host='172.17.0.1', port=6379, db=0)
    job_dict = instantiate_job(jid, status, start, end)
    rd.hmset(generate_job_key(jid), job_dict)
    q.put(jid)
    return job_dict

#problem 3:fetching job from redis 
def get_job_by_id(jid):
    #rd = redis.StrictRedis(host='172.17.0.1', port=6379, db=0)
    requested_job = rd.hgetall(generate_job_key(jid))
    updated_requested_job={}
    for (k,v) in requested_job.items():
        if k.decode("utf-8")!="plot":
            updated_requested_job[k.decode("utf-8")]= v.decode("utf-8")
    
    return updated_requested_job

#problem 4: give me all the keys!!!
def return_all_keys():
    #rd = redis.StrictRedis(host='172.17.0.1', port=6379, db=0)
    alljobs = []
    for key in rd.keys():
        keyencode = key.decode('utf-8')
        stringsplit = keyencode.split(".")
        appending = get_job_by_id(stringsplit[1])
        #appending = {k.decode("utf-8"): v.decode("utf-8") for k,v in appending.items()}
        #alljobs.append(get_job_by_id(stringsplit[1]))
        #print(appending)
        alljobs.append(appending)
        #alljobs.append(eval(rd.get(key.decode('utf-8')).decode('utf-8')))
    return alljobs

def queue_job(jid):
    q.put(jid)

def update_job_status(jid, status):
    job = get_job_by_id(jid)
    job['status'] = status
    rd.hmset(generate_job_key(jid),job)

def execute_job(jid):
    job = get_job_by_id(jid)
    points = get_range_years(startT=int(job['start']),endT=int(job['end']))
    years = []
    spots = []
    for p in points:
        years.append(int(p['year']))
        spots.append(int(p['spots']))
    tmp_file = '/tmp/{}.png'.format(jid)
    plt.figure(1)
    plt.scatter(years,spots)
    plt.xlabel("Years")
    plt.ylabel("Spots")
    plt.title('Plot for job {}'.format(jid))
    plt.savefig(tmp_file, dpi=150)
    job['plot'] = open(tmp_file, 'rb').read()
    rd.hmset(generate_job_key(jid),job)

def get_job_plot(jid):
    job = get_job_by_id(jid)
    if not job['status']==COMPLETE_STATUS:
        return False
    return rd.hmget(generate_job_key(jid), 'plot')





import time 
from hw8 import q, update_job_status, IN_PROGRESS, COMPLETE_STATUS, execute_job

@q.worker 
def work_job(jid):
    update_job_status(jid, IN_PROGRESS)
    execute_job(jid) 
    update_job_status(jid, COMPLETE_STATUS)

work_job()

data_start=1770
data_end=1869

def read_csv():
        sunspot_list = {}
        sunspot_list['s'] = []
        count = 0
        with open('sunspots.csv','r') as csv_file:
            for line in csv_file: 
                sillystring = line.split(',')
                spots = sillystring[1].split('\n')
                sunspot_list['s'].append({
                    'id' : count,
                    'year' : int(sillystring[0]),
                    'spots' : int(spots[0])
                    })
                count = count + 1
        #print(sunspot_list['s']['id' ==1])
        data_start = sunspot_list['s'][0]['year']
        data_end   = sunspot_list['s'][0]['year']
        return(sunspot_list)
def get_all():
	sunspot_list = read_csv()
	for p in sunspot_list['s']:
		print(p)
	return(sunspot_list['s'])

def get_range_years(startT = 1770, endT = 1869):
	if startT < 1770:
		startT = 1770
	if endT > 1869:
		endT = 1869

	sunspot_list = read_csv()
	return_sunspot = list() 
	for p in sunspot_list['s']:
		#print(p['year'])
		if (int(p['year']) >= startT and int(p['year']) <= endT):
			print(p)
			return_sunspot.append(p)
	return(return_sunspot)

def get_range_ID(limit = 99, offset = 1):
	sunspot_list = read_csv()
	return_sunspot = list()
	for p in sunspot_list['s']:
		if (int(p['id']) >= offset and int(p['id']) <= offset+limit-1):
			return_sunspot.append(p)
	return(return_sunspot)

def write_csv(year, spots):
    appending = str(year) + "," + str(spots) + "\n"
    year_prior = year - 1
    
    with open('sunspots.csv', 'r') as csv_file:
        lines = csv_file.readlines()
        count = 0
        for line in lines:
            if line.startswith(str(year_prior)):
                insert_line = count
            else:
                insert_line = -1
            count = count + 1
    
    lines.insert(insert_line,appending)

    with open('sunspots.csv', 'w') as csv_write:
        lines = "".join(lines)
        csv_write.write(lines)
    
    #lines.insert(count - insert_line, appending)
    #csv_file.writelines(lines)
    
    #with open('sunspots.csv','a') as csv_file:
    #    csv_file.write(appending)
    return True  

if __name__ == "__main__":
	
	first_ans = input('A is for all, Y is for year search, I is for ID search\n\n')

	if (first_ans == 'A' or first_ans == 'a'):
		sunlist = get_all();
		print(sunlist[-1])
	elif (first_ans == 'Y' or first_ans == 'y'):
		firstyear = input('First year, please\n')
		secondyear = input('Last year, please\n')
		sunlist = get_range_years(int(firstyear),int(secondyear))
	elif (first_ans == 'I' or first_ans == 'i'):
		startID = input('Limit, please\n')
		endID = input('Offset, please\n')
		sunlist = get_range_ID(int(startID),int(endID))


from flask import Flask, jsonify, request, send_file
from hw1 import get_all, get_range_years, get_range_ID, data_start, data_end, write_csv
from hw8 import generate_jid, generate_job_key,instantiate_job,add_job,get_job_by_id,return_all_keys,get_job_plot
import json 
import io
import statistics

app = Flask(__name__)

@app.route('/sunspots', methods=['GET'])
def get_degrees():
    data = get_all()

    start = request.args.get('start')
    end = request.args.get('end')
    limit = request.args.get('limit')
    offset = request.args.get('offset')
    
    if limit is not None:
        try: 
            limit = int(limit) 
        except:
            return jsonify("Please use integers for limits"),400

    if offset is not None:
        try:
            offset = int(offset)
        except:
            return jsonify("Please use integers for offset"),400

    if start is not None:
        try:
            start = int(start)
        except:
            return jsonify("Please use integers for start year"),400

    if end is not None:
        try:
            end = int(end)
        except:
            return jsonify("Please use integers for end year"),400

    if not offset and not limit and not start and not end:
        return jsonify(data)
    
    if offset and start:
        return jsonify("Can't combine start year and offset"),400
    elif offset and end:
        return jsonify("Can't combine end year and offset"),400
    elif limit and start:
        return jsonify("Can't combine start year and limit"),400
    elif limit and end:
        return jsonify("Can't combine end year and limit"),400


    if offset and not limit:
        return jsonify(get_range_ID(offset=offset))
    elif limit and not offset:
        return jsonify(get_range_ID(limit=limit)) 
    elif limit and offset:
        return jsonify(get_range_ID(limit,offset))

    if start and not end:
        return jsonify(get_range_years(startT = start))
    elif end and not start:
        return jsonify(get_range_years(endT = end))
    elif start and end:
        return jsonify(get_range_years(start, end))

@app.route('/sunspots/<get_id>', methods=['GET'])
def get_ID(get_id):
    #offset = request.args.get('offset')

    if get_id is not None:
        try:
            get_id = int(get_id)
        except:
            return jsonify("Please use integers for ID"),400


    if get_id < 0 :
        return jsonify("Please return a valid id"),400
    
    return jsonify(get_range_ID(1,get_id))

@app.route('/sunspots/year/<year_no>', methods=['GET'])
def get_year(year_no):
    if year_no is not None:
        try:
            year_no = int(year_no)
        except:
            return jsonify("Please use integers for year"),400


    if year_no < data_start :
        return jsonify("That year is out of range"),400

    elif year_no > data_end:
        return jsonify("That year is out of range"),400

    return jsonify(get_range_years(year_no,year_no))

@app.route('/sunspots/year/newdata', methods=['POST'])
def post_new_data():
    if request.method == 'POST':
        newdata = request.get_json(force=True)
        year = newdata.get("year")
        spots = newdata.get("spots")
        if year is not None:
            try:
                year = int(year)
            except: return jsonify("Year must be an integer"),400

        if spots is not None:
            try:
                spots = int(spots)
            except: return jsonify("Spots must be an integer"),400

        if write_csv(year,spots):
            return jsonify("Successful append")
        else:
            return jsonify("Data not appended"),400

@app.route('/jobs', methods=['GET','POST'])
def new_job():
    if request.method == 'POST':
        #try:
        job = request.get_json(force=True)
            #return(jsonify(job))
        #except:
        #    return jsonify("That's not valid JSON \n"), 400
        
        start = job.get('start')
        if start is not None:
            try:
                start = int(start)
            except:
                return jsonify('start parameter must be an integer'),400
        
        end = job.get('end')
        if end is not None:
            try:
                end = int(end)
            except:
                return jsonify('end parameter must be an integer'),400
        
        
        offset = job.get('offset')
        limit = job.get('limit')
        if offset is not None and limit is None:
            return jsonify('Please use limit AND offset'), 400
        elif limit is not None and offset is None:
            return jsonify('Please use limit AND offset'), 400


        #offset = job.get('offset')
        if offset is not None:
            try: 
                offset = int(offset)
            except:
                return jsonify('offset parameter must be an integer'),400
        #newstart = offset + 1770
        #if newstart < 1770:
        #    newstart = 1700
        #elif newstart > 1869:
        #    newstart = 1869

        if offset is not None:
            newstart = offset + data_start
            if newstart < data_start:
                newstart = data_start
            elif newstart > data_end:
                newstart = data_end


        #limit = job.get('limit')
        if limit is not None:
            try:
                limit = int(limit)
            except:
                return jsonify('limit parameter must be an integer'),400
        
        if limit is not None and offset is not None : 
            newend = newstart + limit
            if newend > data_end:
                newend = data_end

        
        #newend = newstart + limit
        #if newend > 1869:
        #    newend = 1869

        if offset and start:
            return jsonify("Can't combine start year and offset"),400
        elif offset and end:
            return jsonify("Can't combine end year and offset"),400
        elif limit and start:
            return jsonify("Can't combine start year and limit"),400
        elif limit and end:
            return jsonify("Can't combine end year and limit"),400
        

        if start and end:
            return jsonify(add_job(generate_jid(),start = start, end = end))
        elif start and not end: 
            return jsonify(add_job(generate_jid(),start = start))
        elif end and not start:
            return jsonify(add_job(generate_jid(),end = end))
        elif not start and not end and not limit and not offset:
            return jsonify(add_job(generate_jid()))


        if limit and offset:
            return jsonify(add_job(generate_jid(),start = newstart, end = newend))
        if limit and not offset:
            return jsonify(add_job(generate_jid(),end = newend))
        if offset and not limit:
            return jsonify(add_job(generate_jid(),start = newstart))


    if request.method == 'GET':
        #rd = redis.StrictRedis(host='172.17.0.1', port=6379, db=0)
        return jsonify(return_all_keys())

@app.route('/jobs/<jobs_id>', methods=['GET'])
def get_job_id(jobs_id):
    if get_job_by_id(jobs_id):
        return jsonify(get_job_by_id(jobs_id))
    else:
        return jsonify("Sorry, that job ID doesn't exist"),400

@app.route('/jobs/<jobs_id>/plot',methods=['GET'])
def job_plot(jobs_id):
    plot = get_job_plot(jobs_id)
    if plot == False:
        return jsonify("Job not completed")
    else:
        return send_file(io.BytesIO(plot[0]),
                         mimetype='image/png',
                         as_attachment=True,
                         attachment_filename='{}.png'.format(jobs_id))

@app.route('/jobs/<jobs_id>/stats', methods=['GET'])
def generate_stats(jobs_id):
    if get_job_by_id(jobs_id):
        job = get_job_by_id(jobs_id)
        start = int(job['start'])
        end = int(job['end'])
        
        all_spots = []
        spotdict = get_range_years(start, end)
        for p in spotdict:
            all_spots.append(p['spots'])

        medi = statistics.median(all_spots)
        mini = min(all_spots)
        maxi = max(all_spots)
        meanie = statistics.mean(all_spots)

        stats = {"minimum" : mini, 
                "median" : medi,
                "mean" : meanie,
                "maximum" : maxi}
        return jsonify(stats)
    else: 
        return jsonify("Sorry, that job ID doesn't exist",400)


if __name__ == '__main__':
    app.run(debug = True, host = '0.0.0.0')
